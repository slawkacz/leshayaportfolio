var app = app || {};

$(function() {
    // Start Models
    app.BehanceUser = new Behance.UserModel({
        user: 'leshaya'
    });
    app.BehanceUser.fetch();
    app.BehanceUser.getProjects();
    
   
    // Start Views
    app.Models = {};
    app.Views = {};
    app.Views.projectViews = {};
    app.Models.projectModels = {};
    app.Views.ProjectsList = Backbone.View.extend({
        tagName: 'ul',
        render:function(){
            _.each(this.model.models, function (project) {
                $(this.el).append(new app.Views.projectRow({
                    model:project
                }).render().el);
            }, this);
            return this;
        }
    });

    app.Views.projectRow = Backbone.View.extend({
        tagName: "li",
        className: "project-row",
        initialize: function() {
            this.render();
        },
        render: function() {
            var template = _.template( $("#projectRow-tmpl").html(), this.model.attributes );
            this.$el.html( template );
            return this;
        }
    });
    app.Views.projectView = Backbone.View.extend({
        tagName: "div",
        className: "project-album",
        projectDetails: $(".project-details"),
        initialize: function() {
            this.render();
        },
        render: function() {
            var template = _.template( $("#projectDetails-tmpl").html(), this.model.attributes );
            this.$el.html( template );
            return this;
        },
        show:function(){
            this.projectDetails.hide();
            this.projectDetails.html(this.$el);
            this.projectDetails.show('slow');
        }
    });
    
    // Start Routers
    app.Router = Backbone.Router.extend({
        routes: {
            "about":                 "about",    // #contact
            "contact":                 "contact",    // #contact
            "project":        "project",  // #project/kiwis
            "project/:projectId":        "project"  // #project/kiwis
        },
        about:function(){
        // handle about view
        },
        contact:function(){
        // handles contact view
        },
        project:function(projectId){
            // handle projects or project page
            if(!this.projectsList) {
                this.projectsList = new app.Views.ProjectsList({
                    model:app.BehanceUser.get('projects')
                });
                $('#projects .project-list').html(this.projectsList.render().el);
            }
            if(projectId) {
                $('#projects .project-details').hide();
                if(!app.Models.projectModels[projectId]) {
                    app.Models.projectModels[projectId] = new Behance.ProjectModel({
                        id: projectId
                    });
                    app.Models.projectModels[projectId].fetch({
                        success:function(){
                            console.log("here");
                            app.Views.projectViews[projectId] = new app.Views.projectView({
                                model:app.Models.projectModels[projectId]
                            });
                            app.Views.projectViews[projectId].show();
                        }
                    });
                    
                } else  app.Views.projectViews[projectId].show();
               
            }
        }
    });
    
    var appReady = function(){
        if(app.BehanceUser.get('projects').models.length) {
            new app.Router();
            Backbone.history.start();
        }
        else          
            setTimeout(appReady,300);
        
    }
    appReady();
});


